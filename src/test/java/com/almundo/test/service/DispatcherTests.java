package com.almundo.test.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.almundo.test.TestApplicationTests;
import com.almundo.test.model.Persona;
import com.almundo.test.model.exception.LlamadaEnEsperaException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationTests.class)
public class DispatcherTests {
	
	/*
	 * Se generan 11 llamadas, la ultima debe lanzar una exception de tipo LlamadaEnEsperaException 
	 */
	@Test(expected = LlamadaEnEsperaException.class)
	public void masDeDiezLLamadasConcurrentes() throws LlamadaEnEsperaException {
		int llamadasConcurrentes = 10;
		int numeroOperadores = 7;
		int numeroSupervisores = 2;
		int numeroDirectores = 1;
		
		Dispatcher dispatcher = new Dispatcher(
				llamadasConcurrentes, 
				numeroOperadores, 
				numeroSupervisores, 
				numeroDirectores);
		
		dispatcher.dispatchCall("000001");
		dispatcher.dispatchCall("000002");
		dispatcher.dispatchCall("000003");
		dispatcher.dispatchCall("000004");
		dispatcher.dispatchCall("000005");
		dispatcher.dispatchCall("000006");
		dispatcher.dispatchCall("000007");
		dispatcher.dispatchCall("000008");
		dispatcher.dispatchCall("000009");
		dispatcher.dispatchCall("000010");
		
		dispatcher.dispatchCall("000011");
	}
	
	@Test(expected = LlamadaEnEsperaException.class)
	public void sinPersonasAtencionDisponibles() throws LlamadaEnEsperaException {
		int llamadasConcurrentes = 10;
		int numeroOperadores = 1;
		int numeroSupervisores = 1;
		int numeroDirectores = 1;
		
		Dispatcher dispatcher = new Dispatcher(
				llamadasConcurrentes, 
				numeroOperadores, 
				numeroSupervisores, 
				numeroDirectores);
		
		dispatcher.dispatchCall("000001");
		dispatcher.dispatchCall("000002");
		dispatcher.dispatchCall("000003");

		dispatcher.dispatchCall("000004");
	}
	
	@Test
	public void siguientePersonaQueAtiendeEsOperador() throws LlamadaEnEsperaException {
		Persona persona;
		
		int llamadasConcurrentes = 10;
		int numeroOperadores = 6;
		int numeroSupervisores = 1;
		int numeroDirectores = 1;
		
		Dispatcher dispatcher = new Dispatcher(
				llamadasConcurrentes, 
				numeroOperadores, 
				numeroSupervisores, 
				numeroDirectores);
		
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000001");
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000002");
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000003");
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000004");
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000005");
		
		persona = dispatcher.encontrarPersonaDisponible();		
		assertEquals(persona.getTipoEmpleado(), "Operador");
		dispatcher.dispatchCall("000006");
		
	}
	
	@Test
	public void siguientePersonaQueAtiendeEsSupervisor() throws LlamadaEnEsperaException {
		
		int llamadasConcurrentes = 10;
		int numeroOperadores = 1;
		int numeroSupervisores = 1;
		int numeroDirectores = 1;
		
		Dispatcher dispatcher = new Dispatcher(
				llamadasConcurrentes, 
				numeroOperadores, 
				numeroSupervisores, 
				numeroDirectores);
		
		dispatcher.dispatchCall("000001");
		
		Persona persona = dispatcher.encontrarPersonaDisponible();
		
		assertEquals(persona.getTipoEmpleado(), "Supervisor");

	}
	
	@Test
	public void siguientePersonaQueAtiendeEsDirector() throws LlamadaEnEsperaException {
		
		int llamadasConcurrentes = 10;
		int numeroOperadores = 1;
		int numeroSupervisores = 1;
		int numeroDirectores = 1;
		
		Dispatcher dispatcher = new Dispatcher(
				llamadasConcurrentes, 
				numeroOperadores, 
				numeroSupervisores, 
				numeroDirectores);
		
		dispatcher.dispatchCall("000001");
		dispatcher.dispatchCall("000002");
		
		Persona persona = dispatcher.encontrarPersonaDisponible();
		
		assertEquals(persona.getTipoEmpleado(), "Director");

	}	

	
}
