package com.almundo.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.almundo.test.service.Dispatcher;

@Configuration
public class AppConfig {
	
	@Autowired
	ConfigValues valoresApp;

	@Bean
	public Dispatcher dispatcher() {
		Dispatcher dispatcher = new Dispatcher(
				valoresApp.getLlamadasConcurrentes(), 
				valoresApp.getNumeroOperadores(), 
				valoresApp.getNumeroSupervisores(), 
				valoresApp.getNumeroDirectores()
		);
		return dispatcher;
	}
}
