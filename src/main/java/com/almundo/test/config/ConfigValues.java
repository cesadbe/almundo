package com.almundo.test.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigValues {
	
	@Value("${app.operadores}")
	private Integer numeroOperadores;
	
	@Value("${app.supervisores}")
	private Integer numeroSupervisores;
	
	@Value("${app.directores}")
	private Integer numeroDirectores;
	
	@Value("${app.llamadasConcurrentes}")
	private Integer llamadasConcurrentes;
	
	@Value("${app.prefijoTelefono}")
	String prefijoTel;

	public Integer getNumeroOperadores() {
		return numeroOperadores;
	}

	public void setNumeroOperadores(Integer numeroOperadores) {
		this.numeroOperadores = numeroOperadores;
	}

	public Integer getNumeroSupervisores() {
		return numeroSupervisores;
	}

	public void setNumeroSupervisores(Integer numeroSupervisores) {
		this.numeroSupervisores = numeroSupervisores;
	}

	public Integer getNumeroDirectores() {
		return numeroDirectores;
	}

	public void setNumeroDirectores(Integer numeroDirectores) {
		this.numeroDirectores = numeroDirectores;
	}

	public Integer getLlamadasConcurrentes() {
		return llamadasConcurrentes;
	}

	public void setLlamadasConcurrentes(Integer llamadasConcurrentes) {
		this.llamadasConcurrentes = llamadasConcurrentes;
	}

	public String getPrefijoTel() {
		return prefijoTel;
	}

	public void setPrefijoTel(String prefijoTel) {
		this.prefijoTel = prefijoTel;
	}
	
	
}
