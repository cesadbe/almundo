package com.almundo.test.rest.dto;

public class LlamadaReq {
	private String numeroTelefonico;

	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}

	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}

	@Override
	public String toString() {
		return "LlamadaReq [numeroTelefonico=" + numeroTelefonico + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroTelefonico == null) ? 0 : numeroTelefonico.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LlamadaReq other = (LlamadaReq) obj;
		if (numeroTelefonico == null) {
			if (other.numeroTelefonico != null)
				return false;
		} else if (!numeroTelefonico.equals(other.numeroTelefonico))
			return false;
		return true;
	}
	
	
}
