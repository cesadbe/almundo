package com.almundo.test.rest;

import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.almundo.test.config.ConfigValues;
import com.almundo.test.rest.dto.LlamadaReq;
import com.almundo.test.rest.dto.LlamadaRes;

@RestController
@RequestMapping("api")
public class LlamadaRestController {
	
	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	private ConfigValues valoresConfig;
	
	private Logger logger = LoggerFactory.getLogger(LlamadaRestController.class);

	@RequestMapping(value = "llamada", method= RequestMethod.POST, produces = "application/json")
	@CrossOrigin(origins = "*")
	
	public LlamadaRes atenderLlamada(@RequestBody LlamadaReq request) {
		LlamadaRes response = new LlamadaRes();
		
		JmsTemplate jmsTemplate = appContext.getBean(JmsTemplate.class);
		jmsTemplate.convertAndSend("callQuee", request);
		
		logger.info("...Se recibe llamada de: " + request.getNumeroTelefonico());
		
		response.setResponse("LLamada del numero " + request.getNumeroTelefonico() + " atendida a las " + new Date(System.currentTimeMillis()));
		return response;
	}
	
	
	@RequestMapping(value = "llamada", method= RequestMethod.GET, produces = "application/json")
	@CrossOrigin(origins = "*")
	
	public LlamadaRes atenderLlamada() {
		LlamadaRes response = new LlamadaRes();
		
		LlamadaReq request = new LlamadaReq();
		request.setNumeroTelefonico(generarTelefonoRandom());
		JmsTemplate jmsTemplate = appContext.getBean(JmsTemplate.class);
		jmsTemplate.convertAndSend("callQuee", request);
		
		logger.info("...Se recibe llamada de: " + request.getNumeroTelefonico());
		
		response.setResponse(new Date(System.currentTimeMillis()) + " - LLamada recibida del numero " + request.getNumeroTelefonico());
		return response;
	}
	
	private String generarTelefonoRandom() {
		Random r = new Random();
		int randomNumber = r.ints(0, (400000000)).limit(1).findFirst().getAsInt();
		
		return valoresConfig.getPrefijoTel() + String.format("%010d", Integer.valueOf(randomNumber));		
	}
}
