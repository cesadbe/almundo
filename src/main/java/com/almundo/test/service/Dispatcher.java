package com.almundo.test.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.almundo.test.model.LlamadaProceso;
import com.almundo.test.model.Persona;
import com.almundo.test.model.exception.LlamadaEnEsperaException;

@Component
public class Dispatcher {
	
	Logger logger = LoggerFactory.getLogger(Dispatcher.class);
	
	ThreadPoolTaskExecutor poolLlamadas;
	int sizePool;
	int numeroOperadores;
	int numeroSupervisores;
	int numeroDirectores;
	Map<Persona, LlamadaProceso> personasAtendiendo;
	LlamadaProceso sinLlamada = new LlamadaProceso(null, null, null);

	public Dispatcher(int sizePool_, int numeroOperadores_, int numeroSupervisores_, int numeroDirectores_) {
		super();
		this.sizePool = sizePool_;
		this.numeroOperadores = numeroOperadores_;
		this.numeroSupervisores = numeroSupervisores_;
		this.numeroDirectores = numeroDirectores_;
		this.poolLlamadas = new ThreadPoolTaskExecutor();
		this.poolLlamadas.setCorePoolSize(sizePool_);
		this.poolLlamadas.setMaxPoolSize(sizePool_);
		this.poolLlamadas.setWaitForTasksToCompleteOnShutdown(false);
		this.poolLlamadas.initialize();
		
		personasAtendiendo = new ConcurrentHashMap<Persona, LlamadaProceso>();
		asignarPersonasQueAtienden();
	}
	
	private void asignarPersonasQueAtienden() {
		for(int i=0; i<this.numeroOperadores; i++) {
			personasAtendiendo.put(new Persona("Operador", i+1), sinLlamada);
		}
		for(int i=0; i<this.numeroSupervisores; i++) {
			personasAtendiendo.put(new Persona("Supervisor", i+1), sinLlamada);
		}
		for(int i=0; i<this.numeroDirectores; i++) {
			personasAtendiendo.put(new Persona("Director", i+1), sinLlamada);
		}
	}
	
	public void dispatchCall(String telefonoLlamada) throws LlamadaEnEsperaException {
		logger.debug("  -- Atención a llamada entrante de : " + telefonoLlamada);
		
		if(esMaximasLlamadasConcurrentes()) {
			logger.debug("$$$$$$$ La mesa de ayuda solo puede manejar concurrentemente " + this.sizePool+ " llamadas. Se interrumpe la atención de la llamada " + telefonoLlamada);
			throw new LlamadaEnEsperaException("   !!! No es posible atender la llamada de " + telefonoLlamada);
		}
		
		if(estanTodosOcupados()) {
			logger.debug("$$$$$$$ La mesa de ayuda está llena. Se interrumpe la atención de la llamada " + telefonoLlamada);
			throw new LlamadaEnEsperaException("   !!! No es posible atender la llamada de " + telefonoLlamada);
		}
		
		Persona disponible = encontrarPersonaDisponible();
		if(disponible==null) {
			logger.debug("$$$$$$$ La mesa de ayuda está llena. Se interrumpe la atención de la llamada " + telefonoLlamada);
			throw new LlamadaEnEsperaException("   !!! No es posible atender la llamada de " + telefonoLlamada);
		}
		
		LlamadaProceso llamada = new LlamadaProceso(disponible, telefonoLlamada, personasAtendiendo);
		personasAtendiendo.replace(disponible, llamada);
		poolLlamadas.execute(llamada);
		logger.debug(" \\\\\\\\\\\\\\\\\\\\ POOL ACTUAL  //////////////////// " + getLlamadasConcurrentes());
	}
	
	private boolean esMaximasLlamadasConcurrentes() {
		return getLlamadasConcurrentes()==this.sizePool;
	}

	public int getLlamadasConcurrentes() {
		return poolLlamadas.getActiveCount();
	}
	
	public boolean estanTodosOcupados() {
		return !personasAtendiendo.containsValue(sinLlamada);
	}
	
	public Persona encontrarPersonaDisponible() {

		Persona disponible;
		
		disponible = encontrarOperadorDisponible();		
		if(disponible!=null) {
			return disponible;
		}
		
		disponible = encontrarSupervisorDisponible();		
		if(disponible!=null) {
			return disponible;
		}
		
		disponible = encontrarDirectorDisponible();		
		if(disponible!=null) {
			return disponible;
		}
		
		return null;

	}
	
	public Persona encontrarOperadorDisponible() {
		return encontrarDisponiblePorTipoPersona("Operador");
	}
	
	public Persona encontrarSupervisorDisponible() {
		return encontrarDisponiblePorTipoPersona("Supervisor");
	}
	
	public Persona encontrarDirectorDisponible() {
		return encontrarDisponiblePorTipoPersona("Director");
	}
	
	public Persona encontrarDisponiblePorTipoPersona(String tipoPersona) {
		return personasAtendiendo.entrySet().stream()
		        .filter(entryMap -> entryMap.getValue().equals(sinLlamada) && 
				tipoPersona.equalsIgnoreCase(entryMap.getKey().getTipoEmpleado()) )
				.map(Map.Entry::getKey)
				.findFirst()
				.orElse(null);
	}
	
	
}
