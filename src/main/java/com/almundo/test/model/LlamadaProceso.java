package com.almundo.test.model;

import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LlamadaProceso implements Runnable {

	private Persona personaAtiende;
	private String telefono;
	private Map<Persona, LlamadaProceso> disponibles;
	
	public LlamadaProceso(Persona persona_, String telefono_, Map<Persona, LlamadaProceso> disponibles_) {
		super();
		this.personaAtiende = persona_;
		this.telefono = telefono_;
		this.disponibles = disponibles_;
	}

	Logger logger = LoggerFactory.getLogger(LlamadaProceso.class);
	
	@Override
	public void run() {
		
		Random r = new Random();
		int randomNumber = r.ints(5000, (11000)).limit(1).findFirst().getAsInt();
		
		logger.debug("   |*|*| " + personaAtiende.getTipoEmpleado() + " # " + 
						personaAtiende.getNumEmpleado() + " inicia atención a llamada de teléfono " + 
						telefono);		
		try {
			Thread.sleep(randomNumber);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		logger.debug("   |-|-| " + personaAtiende.getTipoEmpleado() + " # " + 
				personaAtiende.getNumEmpleado() + " finaliza atención a llamada de teléfono " + 
				telefono);		
		
		disponibles.replace(personaAtiende,  new LlamadaProceso(null, null, null));
	}
	
	public Persona getPersonaAtiende() {
		return personaAtiende;
	}

	public void setPersonaAtiende(Persona personaAtiende) {
		this.personaAtiende = personaAtiende;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((personaAtiende == null) ? 0 : personaAtiende.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LlamadaProceso other = (LlamadaProceso) obj;
		if (personaAtiende == null) {
			if (other.personaAtiende != null)
				return false;
		} else if (!personaAtiende.equals(other.personaAtiende))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		return true;
	}
	
	

}
