package com.almundo.test.model.exception;

public class LlamadaEnEsperaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1697103529671599978L;

	public LlamadaEnEsperaException(String message) {
		super(message);
	}
	

}
