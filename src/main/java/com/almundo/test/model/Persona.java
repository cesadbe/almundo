package com.almundo.test.model;

public class Persona {
	
	private String tipoEmpleado;
	
	private int numEmpleado;
	
	public Persona(String tipoEmpleado, int numEmpleado) {
		super();
		this.tipoEmpleado = tipoEmpleado;
		this.numEmpleado = numEmpleado;
	}

	public String getTipoEmpleado() {
		return tipoEmpleado;
	}
	
	public void setTipoEmpleado(String tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}
	
	public int getNumEmpleado() {
		return numEmpleado;
	}
	
	public void setNumEmpleado(int numEmpleado) {
		this.numEmpleado = numEmpleado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numEmpleado;
		result = prime * result + ((tipoEmpleado == null) ? 0 : tipoEmpleado.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (numEmpleado != other.numEmpleado)
			return false;
		if (tipoEmpleado == null) {
			if (other.tipoEmpleado != null)
				return false;
		} else if (!tipoEmpleado.equals(other.tipoEmpleado))
			return false;
		return true;
	}

}
