package com.almundo.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.almundo.test.rest.dto.LlamadaReq;
import com.almundo.test.service.Dispatcher;

@Component
public class EncoladorLlamadas{
	
	@Autowired
	private ApplicationContext appContext;
	
	Logger logger = LoggerFactory.getLogger(EncoladorLlamadas.class);
	@JmsListener(destination = "callQuee", containerFactory = "myQueeFactory")
	public void recibirLlamada(LlamadaReq llamadaEntrante) throws Exception {
		Dispatcher dispatcher = appContext.getBean(Dispatcher.class);
		dispatcher.dispatchCall(llamadaEntrante.getNumeroTelefonico());
	}

}
