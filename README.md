Ejercicio orientado a la prueba técnica en Java para la compañía AlMundo.

Se basa en una aplicación **SpringBoot** utilizando conceptos de manejo de colas **JMS**, exposicion de servicios **rest** y manejo de concurrencia con **ThreadPoolExecutor** 
---

#Prerequisitos#

- JDK version 8
- Maven o ejecutar el empaquetado portable mvnw 

Opcionalmente 

Importar el proyecto en IDE de preferencia (STS, IntelliJ)

---

#Funcionamiento#

Para las diferentes actividades y fases del proyecto ejecutar en la consola de comandos:

##Compilación##

`mvnw compile`

##Tests##

`mvnw test`

##Ejecución##

`mvnw spring-boot:run`

---

#Descripcion#

El punto de entrada de la aplicación es un servicio Rest de tipo GET el cual simula la entrada de una llamada.

Abrir en el explorador o en alguna herramienta de prueba de servicios Rest como **Postman** la siguiente url:

`http://localhost:8080/api/llamada`

(Preferiblemente usar herramienta externa y no explorador web)

La respuesta de la petición GET es un objeto de la siguiente estructura

```
{
	"response": "Mon Mar 19 19:29:06 COT 2018 - LLamada recibida del numero +54 90197393565"
}
``` 

Una vez el RestController recibe la petición lo que hace es encolar el número telefónico en una cola de mensajes de Apache ActiveMQ con Spring. (Debido a que la configuración de la misma no es objetivo de esta prueba se usa la configuración por defecto, donde la cola intenta 7 veces ejecutar el procesamiento de un mensaje).

De esta manera, la capacidad de atender mas de 10 llamadas o mantener las llamadas en caso que no exista una persona que las atienda, recaen sobre el sistema de mensajes.

El procesamiento de la cola de mensajes de las llamadas se realiza en la clase **EncoladorLlamadas**. En esta clase se realiza el llamado a la clase Dispatcher. En caso que ya existan 10 llamadas concurrentes, o en el caso que no existan personas disponibles para atender la llamada, se propaga una excepcion de tipo **LlamadaEnEsperaException**. De esta manera la llamada no se toma como procesada y vuelve al ciclo de reintento de procesamiento.

La clase Dispatcher posee un Pool de Hilos **ThreadPoolTaskExecutor** y la simulación de la llamada es realizada en el metodo run de la clase **LlamadaProceso**

En el archivo application.properties se pueden configurar los siguientes valores para mayor dinamismo en la simulación:

* `app.operadores`  			Por defecto 7
* `app.supervisores`			Por defecto 2
* `app.directores`				Por defecto 1
* `app.llamadasConcurrentes`	Por defecto 10

Para la revisión de la ejecución se puede observar el output de la consola o en el archivo `/Users/almundo/application.log`

Las convenciones utilizadas en el output son las siguientes:

* `...Se recibe llamada de: +54 90121427541`   Indica el momento cuando una llamada entra en el controlador Rest

* `  -- Atención a llamada entrante de : +54 90121427541` Indica que la llamada ya entró en la ejecución de la cola y se encuentra dentro del llamado del dispatcherCall. Este momento es anterior a la validación de la concurrencia y de la disponibilidad de personas para atender. 

* `  |*|*| Operador # 1 inicia atención a llamada de teléfono +54 90121427541` Cuando hay disponibilidad de llamadas concurrentes y de personas disponibles para atender, indica quien realiza la atención.

* `  |-|-| Operador # 1 finaliza atención a llamada de teléfono +54 90222393099` Indica que la atención a una llamada ha sido finalizada.

* `$$$$$$$ La mesa de ayuda solo puede manejar concurrentemente 10 llamadas. Se interrumpe la atenci¾n de la llamada +54 90329907735` Indica que ya existen 10 conexiones de llamadas siendo atendidas

* ` $$$$$$$ La mesa de ayuda está llena. Se interrumpe la atención de la llamada +54 90068740639` En casos cuando se definen menos de 10 personas que atienden llamadas y todos se encuentran ocupados

* ` ^^^ Se identifica llamada que no se procesa por concurrencia.` Este mensaje se visualiza como handler de errores de la cola de mensajes.

---

#Diagramas#


##Diagrama de componentes##
![Componentes](docs/Diagrama%20de%20Componentes.png)

---

##Autor##

César Adrián Beltrán Villamizar

cesadbe@gmail.com

+57 3105050343

---

##Referencias##

[Spring JMS](https://spring.io/guides/gs/messaging-jms/)

[ThreadPoolTaskExecutor](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/scheduling/concurrent/ThreadPoolTaskExecutor.html)

[Colecciones Concurrentes](https://docs.oracle.com/javase/tutorial/essential/concurrency/collections.html)


